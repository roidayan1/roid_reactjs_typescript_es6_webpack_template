import * as React from "react";
import * as ReactDOM from "react-dom";
import './App.css';

const logo = require("./logo.svg") as string;

let subtitleFunction = function () : string {
  return "ReactJs + Typescript + ES6 + Webpack";
};

const App = () => {

  let subtitle: string = subtitleFunction();

  return (
    <div className="App">
      <div className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h2>RoiD</h2>
        <h3>{subtitle}</h3>
        <h2>Template</h2>
      </div>
      <p className="App-intro">
        To get started, edit <code>src/App.tsx</code> and save to reload.
      </p>
    </div>
  );
};

ReactDOM.render(<App/>,document.getElementById("app"));